/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.dao.UserDao;
import com.werapan.databaseproject.model.Product;
import com.werapan.databaseproject.model.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ProductService {
    private ArrayList<Product> product;
    private ProductDao produceDao = new ProductDao();
    
    
    public ArrayList<Product> getProductsOrderByName(){              
        return (ArrayList<Product>) produceDao.getAll(" product_name asc");
    }
    
    public ArrayList<Product> getProducts(){              
        return (ArrayList<Product>) produceDao.getAll(" product_id asc");
    }
    
    public Product getById(int id){
        ProductDao productDao = new ProductDao();     
        return productDao.get(id);
    }
    
    public Product addNew(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        return productDao.delete(editedProduct);
    }
}
